package com.example.specificationexample.controller;

import com.example.specificationexample.dto.SearchCriteria;
import com.example.specificationexample.model.Student;
import com.example.specificationexample.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/list")
    public Page<Student> getListByCriteria(@RequestBody List<SearchCriteria> dto, Pageable pageable){
        return studentService.getListByCriteria(dto, pageable);
    }
}
