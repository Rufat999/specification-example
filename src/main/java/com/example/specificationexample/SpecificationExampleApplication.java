package com.example.specificationexample;

import com.example.specificationexample.model.Student;
import com.example.specificationexample.repository.StudentRepository;
import com.example.specificationexample.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
@RequiredArgsConstructor
public class SpecificationExampleApplication implements CommandLineRunner {

    private final StudentService studentService;
//    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpecificationExampleApplication.class, args);
    }

//    public int getRandomNumber(int min, int max){
//        return (int) ((Math.random() * (max - min)) + min);
//    }

    @Override
    public void run(String... args) throws Exception {

//        studentService.getAllBy().forEach(System.out::println);
//        System.out.println("===============");
//        studentService.getAllByName().forEach(System.out::println);
//        System.out.println("===============");
//        studentService.getStudentAgeGreaterThanAndGpaLessThan().forEach(System.out::println);

//        for (int i = 0; i < 50; i++) {
//            Student student = Student.builder()
//                    .surname("Abdullayev" + i)
//                    .name("Rufat" + i)
//                    .age(getRandomNumber(20, 30))
//                    .gpa(44.5 + i)
//                    .address("Baku")
//                    .build();
//            studentRepository.save(student);
//        }
    }
}
