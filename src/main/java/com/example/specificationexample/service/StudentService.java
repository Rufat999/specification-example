package com.example.specificationexample.service;

import com.example.specificationexample.dto.SearchCriteria;
import com.example.specificationexample.dto.StudentSpecification;
import com.example.specificationexample.model.Student;
import com.example.specificationexample.repository.StudentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository studentRepository;

    public Page<Student> getListByCriteria(List<SearchCriteria> dto, Pageable pageable) {
        StudentSpecification studentSpecification = new StudentSpecification();
        dto.forEach(searchCriteria -> studentSpecification.add(searchCriteria));
        return studentRepository.findAll(studentSpecification, pageable);
    }

//    public List<Student> getAllBy() {
//        return studentRepository.findAll(ageGreaterThan(25));
//    }
//
//    public List<Student> getAllByName() {
//        return studentRepository.findAll(nameLike("ta"));
//    }
//
//    public List<Student> getStudentAgeGreaterThanAndGpaLessThan(){
//        return studentRepository.findAll(
//                where(ageGreaterThan(25)
//                        .and(gpaLessThan(70)))
//        );
//    }
//
//    private Specification<Student> ageGreaterThan(int age) {
//        return ((root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(root.get(Student.Fields.age), age));
//    }
//
//    private Specification<Student> nameLike(String name) {
//        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Student.Fields.name), "%" + name + "%");
//    }
//
//    private Specification<Student> ageLessThan(int age) {
//        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThan(root.get(Student.Fields.age), age);
//    }
//
//    private Specification<Student> gpaLessThan(double gpa){
//        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThan(root.get(Student.Fields.gpa), gpa);
//    }
}
